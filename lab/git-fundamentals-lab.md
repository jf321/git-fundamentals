# Lab: Git Fundamentals

## Quiz
Be sure to complete the Gradescope quiz first: https://www.gradescope.com/courses/686748/assignments/3937435/

## Git Practice
This week we will go through a very simple git workflow to get you familiar with the process and to confirm that your toolchain works for future assignments.

### Initial `main` Branch Development
1. Make sure that you have git installed, ssh keys configured, and your Duke GitLab account is setup, as detailed in the [first lab](https://gitlab.oit.duke.edu/MedTechPrototyping/medtech-skill-overview/-/blob/main/lab/Software-Install-Tutorials.md?ref_type=heads#git-version-control).
1. Create a new repository on Duke's GitLab server with the project name `bme290_NETID_git_practice` (where `NETID` is your Duke NetID).  
  * Make sure that you select the option to create a README file.
  * :warning: The structure of the name of the repository is important for grading and receiving full credit on this assignment, and it should be set exactly as shown above with your Net ID substituted where shown. 
1. Clone your new repository, which contains the default `README.md` file to your laptop.
2. Delete all of the contents of the `README.md` file and replace it with the following content, substituting your information for the sections in ALL CAPS:
```markdown
# BME 290, Spring 2024: Git Practice
### My Name (Net ID)
ENTER YOUR NAME HERE (NET ID HERE)

### Summer Plans
ENTER YOUR SUMMER PLANS HERE

### Favorite Food
ENTER YOUR FAVORITE FOOD HERE

### Diet Allergies / Preferences
ENTER ANY DIET ALLERGIES / PREFERENCES HERE
```
3. Commit your changes to the `README.md` file, using the following commit message:
```text
Change content to help Dr. Palmeri plan a fun off-campus dinner.
```
4. Push your commit to the GitLab server (remote).  Confirm that you see your changes on the GitLab website.
5. Create a new file in your repository called `grade.csv` and add the following content:
```csv
numerical_grade,letter_grade
100.0,A+
```
6. Add and commit this new file with the following commit message:
```text
Add my desired grade for this class.
```
7. Create an annotated tag for this commit called `v1.0.0`, with the message `"first annotated tag"`.
8. Push this new commit and its associated tag to the GitLab server.  (Note, this will require 2 separate commands.)
9. Confirm that you can see your latest commit and tag on the GitLab website.

### Lets Use a Branch!
10. Create a new branch called `develop` and check it out. 
11. Create a new file in this branch called `color.txt` and edit that file to contain the name of your favorite color.
12. Add and commit this new file, adding a meaningful commit message.
13. Push this new commit on the `develop` branch to the GitLab server.

### Merge Request Time!
14. Create a new Merge Request on the GitLab to merge the `develop` branch into the `main` branch.
15. Approve your Merge Request on the GitLab website and `Merge` your `develop` branch into `main`.  You do **not** want to *squash* or *rebase* your branch commits.
16. Checkout the `main` branch on your laptop and pull the latest changes from the GitLab server. 
17. Confirm that you can see `color.txt` in your `main` branch, along with its associated commit in your git commit history.

### Local Development Branch and Merge
18. Create a new branch called `bugfix` and check it out.
19. Rename `color.txt` to `food.txt` using the [`git mv`](https://git-scm.com/docs/git-mv) command.
20. Edit the contents of `food.txt` to contain your favorite food instead of your favorite color.
21. Add and commit this change with a meaningful commit message.
22. Checkout the `main` branch and merge your `bugfix` branch into `main`.
23. Create an annotated tag for this commit called `v2.0.0`, with the message `"grade this version!"`.
24. Push your latest `main` branch and the latest annotated tag to the GitLab server.
25. Confirm that you can see your latest commit and tag on the GitLab website.  You should **not** see your `bugfix` branch on the GitLab website, as it was only used locally on your laptop.

### Inspect your git history
26. You can checkout `v1.0.0` of your git history using the following command:
```sh
$ git checkout v1.0.0
```
27. After doing this, confirm that you can see the old `color.txt` file and do not see the `food.txt` file.
28. Once you confirm this, you can checkout the latest version of `main` using `git checkout main`.

## What to Submit
Once you have completed all of the steps above and have everything is pushed to the GitLab server, you need to complete the `Git Fundamentals Lab` "quiz" on Gradescope.